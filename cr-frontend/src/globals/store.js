import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";

export const TEST = 'test';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        test: 12
    },
    mutations: {
        [TEST] (state, payload) {
            state.test = payload.testVal;
        }
    },
    plugins: [new VuexPersistence().plugin]
});