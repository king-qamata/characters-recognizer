package ru.beysembaev.charsrecognizer.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import ru.beysembaev.charsrecognizer.model.CharRecNeuron

@Repository
interface CharRecNeuronRepository: JpaRepository<CharRecNeuron, Long>, JpaSpecificationExecutor<CharRecNeuron>