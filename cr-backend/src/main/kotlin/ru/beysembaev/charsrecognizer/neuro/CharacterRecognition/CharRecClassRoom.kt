package ru.beysembaev.charsrecognizer.neuro.CharacterRecognition

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.beysembaev.charsrecognizer.model.CharRecNeuron
import ru.beysembaev.charsrecognizer.model.CharacterPicture
import ru.beysembaev.charsrecognizer.neuro.ClassRoom
import ru.beysembaev.charsrecognizer.service.CharRecService

@Component
class CharRecClassRoom @Autowired constructor(
        val charRecService: CharRecService
): ClassRoom<CharRecNeuron, CharacterPicture>() {
    override fun loadNeurons(): List<CharRecNeuron> {
        TODO("Not yet implemented")
    }

    override fun loadAssets(): List<CharacterPicture> {
        TODO("Not yet implemented")
    }


}