package ru.beysembaev.charsrecognizer.model

import com.github.pozo.KotlinBuilder
import ru.beysembaev.charsrecognizer.neuro.LearningAsset
import javax.persistence.*

// TODO add full filename to the model
@Entity
@Table(name = "cr_character_picture")
@KotlinBuilder
data class CharacterPicture(

        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "character_picture_id_gen")
        @SequenceGenerator(name = "character_picture_id_gen", sequenceName = "character_picture_id_seq", allocationSize = 1)
        override var id: Long = 0,

        @Column(name = "hash")
        var hash: Int? = null

): LearningAsset