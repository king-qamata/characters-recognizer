package ru.beysembaev.charsrecognizer.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.beysembaev.charsrecognizer.model.*
import ru.beysembaev.charsrecognizer.repository.*
import ru.beysembaev.charsrecognizer.service.CharRecService
import ru.beysembaev.charsrecognizer.service.FileService
import java.io.InputStream
import java.util.*

@Service
@Transactional
class CharRecServiceImpl @Autowired constructor(

        val characterPictureRepository: CharacterPictureRepository,
        val charRecNeuronRepository: CharRecNeuronRepository,
        val charRecSynapseRepository: CharRecSynapseRepository,
        val charRecOutputMeaningRepository: CharRecOutputMeaningRepository,
        val charRecInputMeaningRepository: CharRecInputMeaningRepository,
        val flagsRepository: FlagsRepository,
        val fileService: FileService

): CharRecService, BaseNeuroServiceImpl<CharRecNeuron, CharRecSynapse>() {

    // TODO use for a separate folder for character pictures. Set in application properties
    val dir = ""

    override fun createNeuron(layerNumber: Int, position: Int): CharRecNeuron {
        return CharRecNeuron(layer = layerNumber, position = position)
    }


    override fun createSynapse(neuronFromId: Long, neuronToId: Long): CharRecSynapse {
        return CharRecSynapse(neuronFromId = neuronFromId, neuronToId = neuronToId)
    }


    override fun createNetwork(alphabet: List<Char>, hiddenLayersNumber: Int, neuronsPerLayer: Int, picWidth: Int, picHeight: Int) {

        val neuronsLayers = createNeurons(picWidth * picHeight, alphabet.size, hiddenLayersNumber, neuronsPerLayer)
        charRecNeuronRepository.saveAll(neuronsLayers.flatten())

        for (i in 0 until neuronsLayers.size - 1) {
            val synapses = createSynapses(neuronsLayers[i], neuronsLayers[i + 1])
            charRecSynapseRepository.saveAll(synapses)
        }

        createInputMeanings(neuronsLayers[0], picWidth)
        createOutputMeanings(neuronsLayers.last(), alphabet)

        setCreatedFlag(true)
    }


    override fun findAllNeurons(): List<CharRecNeuron> {
        return charRecNeuronRepository.findAll()
    }


    // TODO collect and return not saved pictures, show them to the user
    // TODO save pictures by reading them with ImageIO, then save in the selected format
    override fun savePictures(inputStreams: List<InputStream>) {
        inputStreams.forEach {

            val inputStream = it
            val bytes = inputStream.readBytes()
            inputStream.close()
            val hash = calcHash(bytes)
            val duplicates = characterPictureRepository.findByHash(hash);

            if (duplicates.any { bytes.equals(fileService.getPicture(it.id)) }) {
                return
            }

            val picture = CharacterPicture(hash = hash)
            fileService.savePicture(characterPictureRepository.save(picture), bytes)

        }
    }


    override fun findPictures(pageable: Pageable): Page<CharacterPicture> {
        return characterPictureRepository.findAll(pageable)
    }


    override fun findAllPictures(): List<CharacterPicture> {
        return characterPictureRepository.findAll()
    }


    override fun getPictureFile(id: Long): InputStream? {
        // TODO switch to using fileName from the model
        return fileService.getFile("$dir/$id.bmp")
    }


    override fun deletePictures(pictureIds: List<Long>) {
        pictureIds.forEach {
            characterPictureRepository.deleteById(it)
            // TODO switch to using fileName from the model
            fileService.deleteFile( "$it.bmp")
        }
    }


    override fun getAlphabet(): List<Char> {
        // TODO this is stupid. Just write a native query (put the query string in a separate class!)
        return charRecOutputMeaningRepository.findAllByOrderByMeaningAsc().map(CharRecOutputMeaning::meaning)
    }


    override fun getFlags(): Flags {
        return flagsRepository.findByIdOrNull(1) ?: flagsRepository.save(Flags())
    }


    private fun calcHash(picture: ByteArray): Int {
        // TODO implement this correctly
        return picture.hashCode()
    }


    private fun createInputMeanings(inputNeurons: List<CharRecNeuron>, picWidth: Int) {
        val meanings = ArrayList<CharRecInputMeaning>(inputNeurons.size)

        inputNeurons.forEach {
            val pos = Position(it.position % picWidth, it.position / picWidth)
            meanings.add(CharRecInputMeaning(it.id, pos))
        }

        charRecInputMeaningRepository.saveAll(meanings)
    }


    private fun createOutputMeanings(outputNeurons: List<CharRecNeuron>, alphabet: List<Char>) {
        val meanings = ArrayList<CharRecOutputMeaning>(outputNeurons.size)

        outputNeurons.forEach {
            meanings.add(CharRecOutputMeaning(it.id, alphabet[it.position]))
        }

        charRecOutputMeaningRepository.saveAll(meanings)
    }


    private fun setTaughtFlag(flagValue: Boolean) {
        val flags = getFlags()
        flags.charRecNetworkTaught = flagValue
    }


    private fun setCreatedFlag(flagValue: Boolean) {
        val flags = getFlags()
        flags.charRecNetworkCreated = flagValue
    }
}