package ru.beysembaev.charsrecognizer.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.beysembaev.charsrecognizer.repository.FlagsRepository
import ru.beysembaev.charsrecognizer.service.ApplicationService

@Service
class ApplicationServiceImpl @Autowired constructor(val flagsRepository: FlagsRepository): ApplicationService {

}