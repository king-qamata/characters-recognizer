package ru.beysembaev.charsrecognizer.service

import ru.beysembaev.charsrecognizer.model.CharacterPicture
import java.io.InputStream

interface FileService {
    fun getPicture(id: Long): ByteArray
    fun savePicture(picture: CharacterPicture, bytes: ByteArray)
    fun getFile(fileName: String): InputStream?
    fun deleteFile(fileName: String): Boolean
}