package ru.beysembaev.charsrecognizer.service.impl

import ru.beysembaev.charsrecognizer.neuro.Neuron
import ru.beysembaev.charsrecognizer.neuro.Synapse
import ru.beysembaev.charsrecognizer.service.BaseNeuroService

abstract class BaseNeuroServiceImpl<N: Neuron, S: Synapse<N>>: BaseNeuroService<N> {

    abstract fun createNeuron(layerNumber: Int, position: Int): N
    abstract fun createSynapse(neuronFromId: Long, neuronToId: Long): S

    protected fun createNeurons(inputsNumber: Int, outputsNumber: Int, hiddenLayersNumber: Int, neuronsPerLayer: Int): List<List<N>> {

        val neuronsLayers = ArrayList<List<N>>(hiddenLayersNumber + 2)

        neuronsLayers.add(createLayer(0, inputsNumber))

        for (i in 1 .. hiddenLayersNumber) {
            neuronsLayers.add(createLayer(i, neuronsPerLayer))
        }

        neuronsLayers.add(createLayer(hiddenLayersNumber + 1, outputsNumber))

        return neuronsLayers
    }

    protected fun createSynapses(neuronsFrom: List<N>, neuronsTo: List<N>): List<S> {
        val synapses = ArrayList<S>(neuronsFrom.size * neuronsTo.size)
        neuronsFrom.forEach { neuronFrom ->
            neuronsTo.forEach { neuronTo ->
                synapses.add(createSynapse(neuronFromId = neuronFrom.id, neuronToId = neuronTo.id))
            }
        }
        return synapses
    }

    private fun createLayer(layerNumber: Int, neuronsNumber: Int): List<N> {
        val result = ArrayList<N>(neuronsNumber)

        for (i in 0 until neuronsNumber) {
            result.add(createNeuron(layerNumber, i))
        }

        return result
    }
}