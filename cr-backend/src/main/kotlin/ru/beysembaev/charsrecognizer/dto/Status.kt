package ru.beysembaev.charsrecognizer.dto

import java.io.Serializable

open class Status(val isSuccess: Boolean, val result: Any?): Serializable {
    val timestamp: Long

    init {
        timestamp = System.currentTimeMillis()
    }
}
